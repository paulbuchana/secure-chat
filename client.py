import wx
from twisted.internet import wxreactor
wxreactor.install()

from twisted.internet import ssl, reactor
from twisted.internet.protocol import ClientFactory, Protocol


class ChatGUI(wx.Frame):
    
    def __init__(self):
        wx.Frame.__init__(self, parent = None, title = "CSc Security")
        
        window = wx.BoxSizer(wx.VERTICAL) # Make window frame
        self.text = wx.TextCtrl(self, style = wx.TE_MULTILINE | wx.TE_READONLY) # make central text readonly
        
        self.textSender = wx.TextCtrl(self, style = wx.TE_PROCESS_ENTER, size = (300, 25)) # make editable text
        
        window.Add(self.text, 5, wx.EXPAND)
        window.Add(self.textSender, 0, wx.EXPAND)
        self.SetSizer(window)
        
        self.textSender.Bind(wx.EVT_TEXT_ENTER, self.send) # make action for ediatable text
        
    
    def send(self, action):
        text = str(self.textSender.GetValue())
        self.protocol.sendMessage(text)
        self.textSender.SetValue("")
        

class ChatClient(Protocol):
    def __init__(self):
        self.output = None
        
    def dataReceived(self, data):
        # attaches the gui and the protocol
        gui = self.factory.gui
        gui.protocol = self    
        
        if gui:
            newText = gui.text.GetValue()   # get value from main text
            gui.text.SetValue(newText + data)   # append old data with new
            gui.text.SetInsertionPointEnd() # scrool to the bottom
            
    def connectionMade(self):
        self.output = self.factory.gui.text
    
    def sendMessage(self, message):
        self.transport.write(message)

class ChatFactory(ClientFactory):
    def __init__(self, gui):
        self.gui = gui
        self.protocol = ChatClient
        
    def clientConnectionLost(self, trasport, reason):
        reactor.stop()
        
    def clientConnectionFailed(self, trasport, reason):
        reactor.stop()
        
if __name__ == '__main__':
    app = wx.App(False)
    window = ChatGUI()
    window.Show()
    
    reactor.registerWxApp(app)
    reactor.connectSSL("localhost", 8803, ChatFactory(window), ssl.ClientContextFactory())
    
    reactor.run()